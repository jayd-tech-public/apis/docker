import json
import requests
from requests.auth import HTTPBasicAuth

with open("credentials.json") as f:
    credentials = json.loads(f.read())
basic = HTTPBasicAuth(credentials["username"], credentials["password"])
r = requests.get(f'https://{credentials["repository"]}/v2/_catalog', auth=basic)
print(r.text)
